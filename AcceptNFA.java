import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class AcceptNFA {

    public static boolean accept(EpsilonNFA n, String w){
        Set<State> currentStates = new HashSet<>();
        currentStates.add(n.startState);

        for(int i = 0; i < w.length(); i++) {
            char character = w.charAt(i);
            Set<State> nextStates = new HashSet<>();

            for (State s :
                    currentStates)
                nextStates.addAll(n.getSuccessors(s, character));

            findEpsilonStates(n, nextStates);

            currentStates = nextStates;
        }

        currentStates.retainAll(n.finalStates);

        return currentStates.size() != 0;
    }

    private static void findEpsilonStates(EpsilonNFA n, Set<State> startStates) {
        int startSize = startStates.size();

        for (State state :
                startStates) {
            startStates.addAll(n.getSuccessors(state, Transition.EPSILON));
        }

        if(startStates.size() != startSize)
            findEpsilonStates(n, startStates);
    }
    

    public static void main (String[] args){
        Scanner scanner = new Scanner(System.in);
        EpsilonNFA e = Parser.parse(scanner);
        String word = scanner.nextLine();
        while(!word.equals("DONE")) {
            System.out.println(word + ": " + accept(e, word));
            word = scanner.nextLine();
        }
        scanner.close();
    }
}
